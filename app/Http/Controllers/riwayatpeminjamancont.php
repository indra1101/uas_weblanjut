<?php

namespace App\Http\Controllers;

use App\Models\peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class riwayatpeminjamancont extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $peminjaman = peminjaman::join('users', 'peminjamen.id_user', '=', 'users.id')
        ->join('akun_zooms', 'peminjamen.id_akun_zoom', '=', 'akun_zooms.id')
        ->select('peminjamen.*', 'users.name', 'akun_zooms.email')
        ->where('peminjamen.id_user', '=', Auth::user()->id)
        ->get();

  
        return view('/vriwayatpeminjaman', [
        'peminjamen' => $peminjaman,
        ]);


        // $peminjaman = DB::table('peminjamen')
        // ->join('users', 'peminjamen.id_user', '=', 'users.id')
        // ->join('akun_zooms', 'peminjamen.id_akun_zoom', '=', 'akun_zooms.id')
        // ->select('peminjamen.*', 'users.name', 'akun_zooms.email',)
        // ->where('peminjamen.id_user', '=', Auth::user()->id)
        // ->get();
        // //$jumlah_transaksi = DB::table('transaksi')->count();
        // return view('/vriwayatpeminjaman', [
        // //'id_page' => 'transaksi',
        // 'peminjamen' => $peminjaman,
        // //'jml_transaksi' => $jumlah_transaksi
        // ]);

        // return view('vriwayatpeminjaman');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $peminjaman = peminjaman::find($id);
        $peminjaman->delete();
        
        return redirect('/riwayatpeminjaman');
    }

    public function recent(){
        $peminjaman = peminjaman::join('users', 'peminjamen.id_user', '=', 'users.id')
        ->join('akun_zooms', 'peminjamen.id_akun_zoom', '=', 'akun_zooms.id')
        ->select('peminjamen.*', 'users.name', 'akun_zooms.email')
        ->where('peminjamen.id_user', '=', Auth::user()->id)
        ->onlyTrashed()
        ->get();

  
        return view('/vrecentriwayat', [
        'peminjamen' => $peminjaman,
        ]);
    }

    
}
