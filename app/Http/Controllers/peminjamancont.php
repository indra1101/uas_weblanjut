<?php

namespace App\Http\Controllers;

use App\Models\peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class peminjamancont extends Controller
{
    public function index(){

        $peminjaman = peminjaman::join('users', 'peminjamen.id_user', '=', 'users.id')
        ->join('akun_zooms', 'peminjamen.id_akun_zoom', '=', 'akun_zooms.id')
        ->select('peminjamen.*', 'users.name', 'akun_zooms.email')
        ->get();

  
        return view('/vpeminjaman', [
        'peminjamen' => $peminjaman,
        ]);


        // $peminjaman = DB::table('peminjamen')
        //             ->join('users', 'peminjamen.id_user', '=', 'users.id')
        //             ->join('akun_zooms', 'peminjamen.id_akun_zoom', '=', 'akun_zooms.id')
        //             ->select('peminjamen.*', 'users.name', 'akun_zooms.email',)
        //             ->get();
        // //$jumlah_transaksi = DB::table('transaksi')->count();
        // return view('/vpeminjaman', [
        //     //'id_page' => 'transaksi',
        //     'peminjamen' => $peminjaman,
        //     //'jml_transaksi' => $jumlah_transaksi
        // ]);

    }

    public function edit($id)
    {
        $peminjaman = peminjaman::where('id',$id)->first();
        return view('veditpeminjaman',['peminjaman'=>$peminjaman]);
    }


    public function update(Request $request, $id)
    {
        $peminjaman  = peminjaman::find($id);
        $peminjaman->status_pinjam = $request->status_pinjam;
        $peminjaman->catatan_staf = $request->catatan_staf;

        $peminjaman->save();
        return redirect('/peminjaman');

    }

    public function destroy($id)
    {
        //
        $peminjaman = peminjaman::find($id);
        $peminjaman->delete();
        
        return redirect('/peminjaman');
    }

}
