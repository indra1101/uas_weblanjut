<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class logincont extends Controller
{
    public function postLogin(Request $request){
        // dd($request->all());
        if(Auth::attempt($request->only('email','password'))){
            return redirect('/index');
        }
        return redirect('/login');
    }
    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }
    public function create()
    {
        //
        return view('vregister');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:5|max:255',
            // 'level' => 'required',
        ]);

        $validateData['password'] = Hash::make($validateData['password']);
        $validateData['level'] = $request->level;

        User::create($validateData);
        
        return redirect('/login');

        
        // $users = new user();
        // $users->name = $request->name;
        // $users->email = $request->email;
        // $users->password = $request->password;
        // $users->level = $request->level;

        // $users->save();

    }
}

