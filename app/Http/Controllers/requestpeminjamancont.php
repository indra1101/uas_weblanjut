<?php

namespace App\Http\Controllers;

use App\Models\akun_zoom;
use Illuminate\Http\Request;
use App\Models\peminjaman;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class requestpeminjamancont extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        // return view('vrequestpeminjaman');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $azoom = akun_zoom::all();
        // $azoom = DB::table('akun_zooms')->get();
        return view('/vrequestpeminjaman', ['akun_zoom' => $azoom]);

        // return    view('vtambahakunzoom');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rpeminjaman = new peminjaman();
        $rpeminjaman->id_akun_zoom = $request->id_akun_zoom;
        $rpeminjaman->id_user = Auth::user()->id;
        $rpeminjaman->nama_kegiatan = $request->nama_kegiatan;
        $rpeminjaman->deskripsi = $request->deskripsi;
        $rpeminjaman->tanggal = $request->tanggal;
        $rpeminjaman->jam = $request->jam;
        $rpeminjaman->durasi = $request->durasi;
        $rpeminjaman->catatan_staf = "null";
        $rpeminjaman->status_pinjam = "noaction";

        $rpeminjaman->save();

        return redirect('/akunzoom');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
