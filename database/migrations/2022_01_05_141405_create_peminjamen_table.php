<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeminjamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjamen', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_akun_zoom')->unsigned();
            $table->string('nama_kegiatan',100);
            $table->string('deskripsi',100);
            $table->date('tanggal');
            $table->time('jam', $precision = 0);
            $table->decimal('durasi', $precision = 8, $scale = 2);
            $table->string('catatan_staf',100);
            $table->enum('status_pinjam', ['noaction', 'approved', 'rejected']);
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::table('peminjamen', function($table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_akun_zoom')->references('id')->on('akun_zooms')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjamen');
    }
}
