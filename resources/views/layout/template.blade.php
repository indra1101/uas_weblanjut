<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>@yield('title')</title>
    <!-- header -->
    @include('layout._headerInclude')
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="index.html">Peminjaman Akun Zoom</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i
                class="fas fa-bars"></i></button>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto  me-3 me-lg-4">
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><p class="dropdown-item">{{auth()->user()->name}}</p></li>
                       
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Tabel</div>
                        <a class="nav-link" href="/index">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            DASHBOARD
                        </a>
                        @if (auth()->user()->level=='staff')
                            <a class="nav-link" href="/peminjaman">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                PEMINJAMAN
                            </a>
                        @elseif (auth()->user()->level=='mahasiswa')
                            <a class="nav-link" href="/requestpeminjaman">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                REQUEST PEMINJAMAN
                            </a>
                            <a class="nav-link" href="riwayatpeminjaman">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                RIWAYAT PEMINJAMAN
                            </a>
                        @endif
                        <a class="nav-link" href="/jadwalapproved">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                JADWAL APROVED
                            </a>
                            <a class="nav-link" href="/akunzoom">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                AKUN ZOOM
                            </a>

                        
                        
                        <!-- <a class="nav-link" href="/logout">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            LOGOUT
                        </a> -->
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <div>

                <!-- content -->
                @yield('main')

            </div>
        </div>
    </div>
    <!-- footer -->
    @include('layout._footerInclude')
</body>

</html>
