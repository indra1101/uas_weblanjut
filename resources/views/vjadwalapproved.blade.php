@extends('layout.template')
@section('title','Jadwal Approved')
@section('main')

            <div class="container-fluid px-4">
                <h1 class="mt-4">Jadwal Approved</h1>

                <div class="card mb-4">
                    <div class="card-header d-flex align-items-center justify-content-between small">
                        <div>
                            <i class="fas fa-table me-1"></i>
                            Jadwal Approved
                        </div>
                        <!-- <div>
                            <a href="" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                        </div> -->

                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Akun Zoom</th>
                                    <th>Nama Kegiatan</th>
                                    <th>Deskripsi</th>
                                    <th>Tanggal</th>
                                    <th>Jam</th>
                                    <th>Durasi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($peminjamen as $peminjaman)
                                <tr>
                                    
                                    <td> {{ $peminjaman->email }} </td>
                                    <td> {{ $peminjaman->nama_kegiatan }} </td>
                                    <td> {{ $peminjaman->deskripsi }} </td>
                                    <td> {{ $peminjaman->tanggal }} </td>
                                    <td> {{ $peminjaman->jam }} </td>
                                    <td> {{ $peminjaman->durasi }} </td>

                                </tr>

                            @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


@endsection
