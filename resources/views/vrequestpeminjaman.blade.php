@extends('layout2.template')
@section('title','Request Peminjaman')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Request Peminjaman</h3></div>
                    <div class="card-body">
                        <form action="/requestpeminjaman/store" method="POST">
                            {{ csrf_field() }}
                            <div class="form-floating mb-3">
                                <select name="id_akun_zoom" id="id_akun_zoom"  class="selectpicker form-control" data-live-search="true" type="text" placeholder="Email" required>
                                    <option value="">Pilih Email</option>
                                    @foreach ($akun_zoom as $azoom)
                                        <option value="{{$azoom->id}} "> {{$azoom->email}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="nama_kegiatan" class="form-control" type="text" placeholder="Nama Kegiatan" />
                                <label>Nama Kegiatan</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="deskripsi" class="form-control" type="text" placeholder="Deskripsi" />
                                <label>Deskripsi</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="tanggal" class="form-control" type="date" placeholder="Tanggal" />
                                <label>Tanggal</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="jam" class="form-control" type="time" placeholder="Jam" />
                                <label>Jam</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="durasi" class="form-control" type="number"step="any" placeholder="Durasi" />
                                <label>Durasi</label>
                            </div>

                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " href="" value="Create" type="submit">

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
