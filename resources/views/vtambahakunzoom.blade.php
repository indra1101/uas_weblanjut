@extends('layout2.template')
@section('title','Tambah Akun Zoom')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Akun Zoom</h3></div>
                    <div class="card-body">
                        <form action="tambahakunzoom/store" method="POST">
                            {{ csrf_field() }}
                            <div class="form-floating mb-3">
                                <input name="email" class="form-control" type="text" placeholder="email" required />
                                <label>Email</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="password" class="form-control" type="text" placeholder="password" required/>
                                <label>Password</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="kapasitas" class="form-control" type="number" placeholder="kapasitas" required/>
                                <label>Kapasitas</label>
                            </div>

                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " href="" value="Create" type="submit">
                                {{-- <div class="d-grid"><a class="btn btn-primary btn-block" href="login.html">Tambah Data</a></div> --}}
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
