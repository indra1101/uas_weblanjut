@extends('layout.template')
@section('title','Riwayat Peminjaman')
@section('main')

            <div class="container-fluid px-4">
                <h1 class="mt-4">Riwayat Peminjaman</h1>

                <div class="card mb-4">
                    <div class="card-header d-flex align-items-center justify-content-between small">
                        <div>
                            <i class="fas fa-table me-1"></i>
                            Riwayat Peminjaman
                        </div>
                        
                        <div>
                            <a href="/riwayatpeminjaman" class="btn btn-primary "><i class="fa fa-chevron-left" aria-hidden="true"></i>Back</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Email Zoom</th>
                                    <th>Nama Kegiatan</th>
                                    <th>Deskripsi</th>
                                    <th>Tanggal</th>
                                    <th>Jam</th>
                                    <th>Durasi</th>
                                    <th>keterangan staf</th>
                                    <th>Status Pinjam</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($peminjamen as $peminjaman)
                                <tr>
                                    <td> {{ $peminjaman->email }} </td>
                                    <td> {{ $peminjaman->nama_kegiatan }} </td>
                                    <td> {{ $peminjaman->deskripsi }} </td>
                                    <td> {{ $peminjaman->tanggal }} </td>
                                    <td> {{ $peminjaman->jam }} </td>
                                    <td> {{ $peminjaman->durasi }} </td>
                                    <td> {{ $peminjaman->catatan_staf }} </td>
                                    <td> {{ $peminjaman->status_pinjam }} </td>
                                </tr>

                            @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


@endsection
